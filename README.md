# Java Challenge

Link Gitlab: https://gitlab.com/bayubagusbagaswara/java-challenge-bayu

# Register 

- POST, `/api/auth/register`, request body

# Login

- POST, `/api/auth/login`, request body

# User

- Create User : POST, `/api/users/save`, request body
- Get User By Id : GET, `/api/users/{id}`
- Get User By Username : GET, `/api/users/username`, param : username
- Get User By Username Like : GET, `/api/users/username/like`, param : username
- Get All Users : GET, `/api/users`
- Get All User Pagination : GET, `/api/users/page`, param : pageNo, pageSize, sortBy, sortDir
- Update User : PUT, `/api/users/{id}`, request body
- Delete User : DELETE, `/api/users/{id}`