package com.ist.challenge.bayu.controller;

import com.ist.challenge.bayu.dto.LoginRequest;
import com.ist.challenge.bayu.dto.MessageResponse;
import com.ist.challenge.bayu.dto.RegisterRequest;
import com.ist.challenge.bayu.exception.BadRequestException;
import com.ist.challenge.bayu.exception.UnauthorizedException;
import com.ist.challenge.bayu.exception.UsernameAlreadyExistsException;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AuthControllerErrorTest {

    private final static Logger log = LoggerFactory.getLogger(AuthControllerErrorTest.class);

    @Autowired
    AuthController authController;

    @Test
    @Order(1)
    void registerFailedUsernameIsExists() {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setUsername("bagus");
        registerRequest.setPassword("bagus1234567");

        assertThrows(UsernameAlreadyExistsException.class, () -> {
            ResponseEntity<MessageResponse> register = authController.register(registerRequest);
        });
    }

    @Test
    @Order(2)
    void loginFailedUsernameOrPasswordIsBlank() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername("");
        loginRequest.setPassword("");

        assertThrows(BadRequestException.class, () -> {
            ResponseEntity<MessageResponse> login = authController.login(loginRequest);
        });
    }

    @Test
    @Order(3)
    void loginFailedPasswordDoesNotMatchWithUsername() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername("bagus");
        loginRequest.setPassword("bagus1234");

        assertThrows(UnauthorizedException.class, () -> {
            ResponseEntity<MessageResponse> login = authController.login(loginRequest);
        });
    }

}