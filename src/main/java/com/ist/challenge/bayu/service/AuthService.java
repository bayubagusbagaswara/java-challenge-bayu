package com.ist.challenge.bayu.service;

import com.ist.challenge.bayu.dto.LoginRequest;
import com.ist.challenge.bayu.dto.LoginResponse;
import com.ist.challenge.bayu.dto.RegisterRequest;
import com.ist.challenge.bayu.dto.RegisterResponse;

public interface AuthService {

    RegisterResponse register(RegisterRequest registerRequest);

    LoginResponse login(LoginRequest loginRequest);
}
