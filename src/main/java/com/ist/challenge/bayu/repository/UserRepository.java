package com.ist.challenge.bayu.repository;

import com.ist.challenge.bayu.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("SELECT u FROM User u WHERE u.username = :username")
    Optional<User> getByUsername(@Param("username") String username);

    @Query("SELECT COUNT(u) > 0 FROM User u WHERE u.username = :username")
    Boolean checkUsername(String username);

    List<User> findByUsernameContainingIgnoreCase(String username);

    @Query("SELECT u FROM User u WHERE LOWER(u.username) LIKE LOWER(CONCAT('%', :username, '%'))")
    List<User> getByUsernameContainingIgnoreCase(@Param("username") String username);

    @Query("SELECT u FROM User u WHERE u.username LIKE %:username%")
    List<User> searchByUsernameLike(@Param("username") String username);

    @Query("SELECT u FROM User u")
    Page<User> findAllWithPagination(Pageable pageable);
}
